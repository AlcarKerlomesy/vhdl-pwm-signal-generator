-- VHDL 2008
library IEEE, std;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.math_real.ALL;
use IEEE.std_logic_textio.all ;
use std.textio.all ;

entity test_RC_servo is
--  Port ( );
end test_RC_servo;

architecture Behavioral of test_RC_servo is
    component RC_servo
        Generic ( IS_SIMULATION : boolean := false);
        Port ( zero_one, one, five, neutral : in STD_LOGIC;
           add, enable, show_hex : in STD_LOGIC;
           clk : in STD_LOGIC;
           pwm_out : out STD_LOGIC;
           enable_seg : out STD_LOGIC_VECTOR (3 downto 0);
           seg_out : out STD_LOGIC_VECTOR (6 downto 0);
           dot : out STD_LOGIC);
    end component;

    signal zero_one, one, five, neutral, enable, show_hex, clk : STD_LOGIC := '0' ;
    signal add : std_logic := '0' ;
    signal pwm_out, dot : std_logic ;
    signal enable_seg : STD_LOGIC_VECTOR (3 downto 0);
    signal seg_out : STD_LOGIC_VECTOR (6 downto 0);
    
    constant width_rel_size : integer := integer(ceil(log2(real(1800)))) ;
    signal pwm_width_bcd : unsigned (15 downto 0) ;
    signal pwm_width_rel : unsigned (width_rel_size - 1 downto 0) := (others => '0') ;
    
    constant pwm_min_width : integer := 0 ;
begin
    tested_component: RC_servo
        Generic map ( IS_SIMUlATION => true)
        Port map (zero_one => zero_one, one => one, five => five, neutral => neutral,
            add => add, enable => enable, show_hex => show_hex, clk => clk,
            pwm_out => pwm_out, enable_seg => enable_seg, seg_out => seg_out, dot => dot) ;
    
    pwm_width_bcd <= << signal tested_component.pwm_width_bcd : unsigned (15 downto 0) >> ;
    pwm_width_rel <= << signal tested_component.pwm_width_rel : unsigned (width_rel_size - 1 downto 0) >> ;
    
    clk_gen: process
    begin
        wait for 2 ns ;
        clk <= not clk ;
        wait for 3 ns ;
    end process clk_gen ;
    
    test_proc : process
        file test_file : text ;
        variable t_line : line ;
        variable t_width_rel : std_logic_vector (width_rel_size-1 downto 0) ;
        variable t_digit : std_logic_vector (3 downto 0) ;
        variable m2 : integer ;
    begin
        report "Start simulation" severity NOTE ;
        file_open(test_file, "test_file.txt", read_mode) ;
        
        while not endfile(test_file) loop
            readline(test_file, t_line) ;
            
            hread(t_line, t_digit) ;
            zero_one <= t_digit(0) ;
            one <= t_digit(1) ;
            five <= t_digit(2) ;
            neutral <= t_digit(3) ;
            hread(t_line, t_digit) ;
            add <= t_digit(0) ;
            wait for 20 ns ;
            
            zero_one <= '0' ;
            one <= '0' ;
            five <= '0' ;
            neutral <= '0' ;
            wait for 20 ns ;
            
            hread(t_line, t_width_rel) ;
            assert unsigned(t_width_rel) = pwm_width_rel report
                "Neutral is " & std_logic'image(neutral) & ", zero_one is " & std_logic'image(zero_one) &
                ", one is " & std_logic'image(one) & " and five is " & std_logic'image(five) & ". " &
                "The output is " & integer'image(to_integer(pwm_width_rel)) &
                ", but it should be " & integer'image(to_integer(unsigned(t_width_rel)))
                severity FAILURE ;
            
            for m in 0 to 3 loop
                m2 := 3 - m ;
                hread(t_line, t_digit) ;
                
                assert to_integer(unsigned(t_digit)) = pwm_width_bcd(4*m2+3 downto 4*m2) report
                    "The displayed number should be " & integer'image(to_integer(pwm_width_rel) + pwm_min_width) &
                    ", but the digit " & integer'image(m2) &
                    " is " & integer'image(to_integer(pwm_width_bcd(4*m2+3 downto 4*m2))) &
                    "The digit 3 is the left-most one and digit 0 is the right-most one."
                    severity FAILURE ;
            end loop ;
            
            wait for 10 ns ;
        end loop ;
        
        file_close(test_file) ;
        report "End of the simulation" severity NOTE ;
        std.env.finish ;
    end process test_proc ;

end Behavioral;
