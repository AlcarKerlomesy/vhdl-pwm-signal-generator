library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Add_Sub_Digit is
    Port ( digit_1 : in unsigned (3 downto 0);
           digit_2 : in unsigned (3 downto 0);
           carry_in : in STD_LOGIC;
           add : in STD_LOGIC;
           digit_out : out unsigned (3 downto 0);
           carry_out : out STD_LOGIC);
end Add_Sub_Digit;

architecture Behavioral of Add_Sub_Digit is
begin

    main_proc: process(digit_1, digit_2, carry_in, carry_in, add)
        variable digit_1_ext : unsigned (4 downto 0) ;
        variable digit_2_ext : unsigned (4 downto 0) ;
        variable op_result : unsigned (4 downto 0) ;
    begin
        digit_1_ext := '0' & digit_1 ;
        digit_2_ext := '0' & digit_2 ;
        carry_out <= '0' ;
    
        if (add = '1') then
            if (carry_in = '1') then
                op_result := digit_1_ext + digit_2_ext + 1 ;
            else
                op_result := digit_1_ext + digit_2_ext ;
            end if ;
            
            if (op_result > 9) then
                op_result := op_result - 10 ;
                carry_out <= '1' ;
            end if ;
        else
            if (carry_in = '1') then
                op_result := digit_1_ext - digit_2_ext - 1 ;
            else
                op_result := digit_1_ext - digit_2_ext ;
            end if ;
            
            if (op_result > 9) then
                op_result := op_result - 22 ;
                carry_out <= '1' ;
            end if ;
        end if ;
        
        digit_out <= op_result(3 downto 0) ;
    end process main_proc ;

end Behavioral;
