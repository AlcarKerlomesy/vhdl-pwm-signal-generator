library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.math_real.ALL;
library work;
use work.pwm_counter_proc.ALL;

entity test_Generate_PWM is
--  Port ( );
end test_Generate_PWM;

architecture Behavioral of test_Generate_PWM is
    component Generate_PWM
        Generic (
            max_count : integer := 100 ;     -- the clock frequency is 100 MHz / max_count
            min_count : integer := 5 ;
            inc_count : integer := 2 ;     -- The pulse width (in clock cycle) is min_count + falling_edge_count*inc_count
            rep_1_count : integer := 1 ;
            rep_2_count : integer := 0 ;
            bit_depth : integer := 16 );
        Port ( falling_edge_count : in unsigned (bit_depth-1 downto 0);
               clk : in STD_LOGIC ;
               reset : in STD_LOGIC ;
               PWM_out : out STD_LOGIC);
    end component;

    constant pwm_resolution : integer := 1800 ;
    constant clk_freq : real := 100000000.0 ;
    constant pwm_freq : real := 50.0 ;
    constant pwm_min_ratio : real := 0.05 ;
    constant pwm_max_ratio : real := 0.1 ;
    constant pwm_count : t_PWM_COUNT := variable_increment(
        clk_freq => clk_freq, pwm_freq => pwm_freq, pwm_min_ratio => pwm_min_ratio, pwm_max_ratio => pwm_max_ratio, pwm_resolution => pwm_resolution) ;

    constant width_rel_size : integer := integer(ceil(log2(real(pwm_resolution)))) ;
    signal pwm_width_rel : unsigned (width_rel_size-1 downto 0) := TO_UNSIGNED(1328, width_rel_size) ;
    
    signal clk : std_logic := '0' ;
    signal pwm_out : std_logic ;
begin

    pwm_gen: Generate_PWM
        generic map (max_count => pwm_count.pwm_maxcount, min_count => pwm_count.pwm_count_min,
            inc_count => pwm_count.pwm_count_inc, bit_depth => width_rel_size,
            rep_1_count => pwm_count.pwm_rep_1, rep_2_count => pwm_count.pwm_rep_2)
        port map (falling_edge_count => pwm_width_rel, clk => clk, reset => '0', PWM_out => pwm_out) ;
    
    clk_gen: process
    begin
        wait for 2 ns ;
        clk <= not clk ;
        wait for 3 ns ;
    end process clk_gen ;
    
    check_pwm_count: process
        constant pwm_maxcount_r : real := round(clk_freq / pwm_freq) ;
        constant pwm_count_min_r : integer := integer(ceil(pwm_maxcount_r*pwm_min_ratio)) ;
        constant pwm_count_max_r : integer := integer(floor(pwm_maxcount_r*pwm_max_ratio)) ;
        constant pwm_count_inc_r : integer := integer(floor(real(pwm_count_max_r - pwm_count_min_r) / real(pwm_resolution))) ;
        variable m : integer ;
        variable m2 : integer ;
        variable switch_inc : std_logic := '0' ;
        variable count : integer ;
    begin
        report "Max count (one period): " & integer'image(integer(pwm_maxcount_r)) ;
        report "Count for the edges of the PWM signal: " & integer'image(pwm_count_min_r) &
            " to " & integer'image(pwm_count_max_r) ;
            
        m := 0 ;
        m2 := 0 ;
        count := 0 ;
        while (m < pwm_resolution) loop
            if (switch_inc = '0') then
                count := count + pwm_count.pwm_count_inc ;
                m2 := m2 + 1 ;
                if (m2 = pwm_count.pwm_rep_1) then
                    switch_inc := '1' ;
                    m2 := 0 ;
                end if ;
            else
                count := count + pwm_count.pwm_count_inc + 1 ;
                m2 := m2 + 1 ;
                if (m2 = pwm_count.pwm_rep_2) then
                    switch_inc := '0' ;
                    m2 := 0 ;
                end if ;
            end if ;
            m := m + 1 ;
        end loop ;
        
        if (count = pwm_count_max_r - pwm_count_min_r) then
            report "The function ""variable_increment"" gives the correct values." ;
        else
            report "ERROR: The falling edge is at " & integer'image(count) &
                ", but it should be at " & integer'image(pwm_count_max_r - pwm_count_min_r) ;
        end if ;
        
        wait ;
    end process check_pwm_count ;

end Behavioral;
