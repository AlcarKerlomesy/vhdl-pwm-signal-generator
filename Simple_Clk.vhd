library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Simple_Clk is
    Generic (
        max_count : integer := 99) ;   -- the clock frequency is 100 MHz / max_count
    Port ( clk_in : in STD_LOGIC;
           reset : in STD_LOGIC;
           clk_out : out STD_LOGIC);
end Simple_Clk;

architecture Behavioral of Simple_Clk is
    constant bit_depth : integer := integer(ceil(log2(real(max_count)))) ;
    signal counter : unsigned (bit_depth-1 downto 0) := (others => '0') ;
    signal clk : std_logic := '0' ;
begin

    clk_out <= clk ;

    counter_proc: process(clk_in)
    begin
        if (falling_edge(clk_in)) then
            counter <= (others => '0') ;
            clk <= '0' ;
        
            if (reset = '1') then
                null ;
            elsif (counter = max_count - 1) then
                clk <= '1' ;
            else
                counter <= counter + 1 ;
            end if ;
        end if ;
    end process counter_proc ; 

end Behavioral;
