library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.math_real.ALL;
library work;
use work.pwm_counter_proc.ALL;

entity RC_servo is
    Generic ( IS_SIMULATION : boolean := false);
    Port ( zero_one, one, five, neutral : in STD_LOGIC;
           add, enable, show_hex : in STD_LOGIC;
           clk : in STD_LOGIC;
           pwm_out : out STD_LOGIC ;
           enable_seg : out STD_LOGIC_VECTOR (3 downto 0) ;
           seg_out : out STD_LOGIC_VECTOR (6 downto 0);
           dot : out STD_LOGIC);
end RC_servo;

architecture Behavioral of RC_servo is
    -- Components
    component Simple_Clk
        Generic (
            max_count : integer := 99) ;   -- the clock frequency is 100 MHz / max_count
        Port ( clk_in : in STD_LOGIC;
               reset : in STD_LOGIC;
               clk_out : out STD_LOGIC);
    end component;
    
    component Hex_to_7_Seg
        Port ( hex : in STD_LOGIC_VECTOR (3 downto 0);
               seven_seg : out STD_LOGIC_VECTOR (6 downto 0));
    end component;
    
    component Add_Sub_Digit
        Port ( digit_1 : in unsigned (3 downto 0);
               digit_2 : in unsigned (3 downto 0);
               carry_in : in STD_LOGIC;
               add : in STD_LOGIC;
               digit_out : out unsigned (3 downto 0);
               carry_out : out STD_LOGIC);
    end component;
    
    component Generate_PWM
        Generic (
            max_count : integer := 100 ;     -- the clock frequency is 100 MHz / max_count
            min_count : integer := 5 ;
            inc_count : integer := 2 ;     -- The pulse width (in clock cycle) is min_count + falling_edge_count*inc_count
            rep_1_count : integer := 1 ;
            rep_2_count : integer := 0 ;
            bit_depth : integer := 16 );
        Port ( falling_edge_count : in unsigned (bit_depth-1 downto 0);
               clk : in STD_LOGIC ;
               reset : in STD_LOGIC ;
               PWM_out : out STD_LOGIC);
    end component;
    
    -- Clock
    constant clk_freq : real := 100000000.0 ;
    constant seg_freq : real := 800.0 ;
    constant seg_maxcount : integer := integer(round(clk_freq / seg_freq)) ;
    
    -- 7 seg
    constant num_digit : integer := 4 ;
    constant dot_pos : integer := 1 ;
    signal seg_clk : std_logic := '0' ;
    signal enable_seg_reg : std_logic_vector (num_digit-1 downto 0) := "0111" ;
    
    -- PWM constants
    constant pwm_min_width : integer := 0 ;         -- 0�
    constant pwm_min_width_bcd : unsigned (4*num_digit-1 downto 0) := (others => '0') ;
    constant pwm_max_width : integer := 1800 ;      -- 180�
    constant pwm_max_width_bcd : unsigned (4*num_digit-1 downto 0) := "0001100000000000" ;
    constant pwm_neutral_width : integer := 900 ;   -- 90�
    constant pwm_neutral_width_bcd : unsigned (4*num_digit-1 downto 0) := "0000100100000000" ;
    
    constant pwm_zero_one_rel_inc : integer := 1 ;
    constant pwm_zero_one_bcd_inc : unsigned (4*num_digit-1 downto 0) := "0000000000000001" ;
    constant pwm_one_rel_inc : integer := 10 ;
    constant pwm_one_bcd_inc : unsigned (4*num_digit-1 downto 0) := "0000000000010000" ;
    constant pwm_five_rel_inc : integer := 50 ;
    constant pwm_five_bcd_inc : unsigned (4*num_digit-1 downto 0) := "0000000001010000" ;
    
    constant pwm_width_range : integer := pwm_max_width - pwm_min_width ;
    constant pwm_neutral_rel : integer := pwm_neutral_width - pwm_min_width ;
    
    -- Master clock: 100 MHz, PWM signal: 50 Hz (20 ms), PWM min: 0.05*20 = 1 ms, PWM max: 0.1*20 = 2 ms
    constant pwm_count : t_PWM_COUNT := variable_increment(
        clk_freq => clk_freq, pwm_freq => 50.0, pwm_min_ratio => 0.05, pwm_max_ratio => 0.1, pwm_resolution => pwm_width_range) ;
    -- Master clock: 100 MHz, PWM signal: 50 Hz (20 ms), PWM min: 0 ms, PWM max: 20 ms
--    constant pwm_count : t_PWM_COUNT := variable_increment(
--        clk_freq => clk_freq, pwm_freq => 50.0, pwm_min_ratio => 0.0, pwm_max_ratio => 1.0, pwm_resolution => pwm_width_range) ;
        
    -- PWM counter
    signal prev_pwm_width_bcd : unsigned (4*num_digit-1 downto 0) := pwm_neutral_width_bcd ;
    signal pwm_width_bcd : unsigned (4*num_digit-1 downto 0) := pwm_neutral_width_bcd ;
    signal op_bcd : unsigned (4*num_digit-1 downto 0) := (others => '0') ;
    signal carry : std_logic_vector (num_digit downto 0) := (others => '0') ;
    
    constant width_rel_size : integer := integer(ceil(log2(real(pwm_width_range)))) ;
    signal pwm_width_rel : unsigned (width_rel_size - 1 downto 0) := TO_UNSIGNED(pwm_neutral_width, width_rel_size) ;
    
    signal displayed_bcd : std_logic_vector (3 downto 0) := (others => '0') ;
    
    -- Button control
    signal prev_button : std_logic := '0' ;
    signal five_reg, one_reg, zero_one_reg : std_logic := '0' ;
begin
    --------------------------- Updating pwm_width_bcd and pwm_main_value  ---------------------------
    carry(0) <= '0' ;
    add_sub_digit_loop: for m in 0 to num_digit-1 generate
        add_sub: Add_Sub_Digit
        port map (digit_1 => prev_pwm_width_bcd(num_digit*(m+1)-1 downto num_digit*m),
            digit_2 => op_bcd(num_digit*(m+1)-1 downto num_digit*m),
            carry_in => carry(m), add => add,
            digit_out => pwm_width_bcd(num_digit*(m+1)-1 downto num_digit*m),
            carry_out => carry(m+1));
    end generate ;
    
    update_bcd: process(clk)
        variable width_rel_inc : integer range 0 to 50 ;
        variable width_rel_temp, width_rel_ext : unsigned (width_rel_size downto 0) ;
    begin
        if (rising_edge(clk)) then if (enable = '0') then
            -- Without those 3 lines, unstable behavior is observed on the real device
            five_reg <= five ;
            one_reg <= one ;
            zero_one_reg <= zero_one ;
            
            prev_button <= zero_one_reg or one_reg or five_reg ;
            
            op_bcd <= (others => '0') ;
            width_rel_inc := 0 ;
            width_rel_ext := '0' & pwm_width_rel ;
            
            prev_pwm_width_bcd <= pwm_width_bcd ;
            
            if neutral = '1' then
                prev_pwm_width_bcd <= pwm_neutral_width_bcd ;
                pwm_width_rel <= TO_UNSIGNED(pwm_neutral_rel, width_rel_size) ;
            else
                if (zero_one_reg and not prev_button) = '1' then
                    op_bcd <= pwm_zero_one_bcd_inc ;
                    width_rel_inc := pwm_zero_one_rel_inc ;
                end if ;
                if (one_reg and not prev_button) = '1' then
                    op_bcd <= pwm_one_bcd_inc ;
                    width_rel_inc := pwm_one_rel_inc ;
                end if ;
                if (five_reg and not prev_button) = '1' then
                    op_bcd <= pwm_five_bcd_inc ;
                    width_rel_inc := pwm_five_rel_inc ;
                end if ;
                
                if add = '1' then
                    width_rel_temp := width_rel_ext + width_rel_inc ;
                    if width_rel_temp > pwm_width_range then
                        op_bcd <= (others => '0') ;
                        prev_pwm_width_bcd <= pwm_max_width_bcd ;
                        pwm_width_rel <= TO_UNSIGNED(pwm_width_range, width_rel_size) ;
                    else
                        pwm_width_rel <= width_rel_temp(width_rel_size-1 downto 0) ;
                    end if ;
                else
                    width_rel_temp := width_rel_ext - width_rel_inc ;
                    if width_rel_temp(width_rel_size) = '1' then
                        op_bcd <= (others => '0') ;
                        prev_pwm_width_bcd <= pwm_min_width_bcd ;
                        pwm_width_rel <= (others => '0') ;
                    else
                        pwm_width_rel <= width_rel_temp(width_rel_size-1 downto 0) ;
                    end if ;
                end if ;
            end if ;
        end if ; end if ;
    end process update_bcd ;

    --------------------------- Generate PWM signal (input: pwm_width_rel)  ---------------------------
    pwm_gen: Generate_PWM
        generic map (max_count => pwm_count.pwm_maxcount, min_count => pwm_count.pwm_count_min,
            inc_count => pwm_count.pwm_count_inc, bit_depth => width_rel_size,
            rep_1_count => pwm_count.pwm_rep_1, rep_2_count => pwm_count.pwm_rep_2)
        port map (falling_edge_count => pwm_width_rel, clk => clk, reset => enable, PWM_out => pwm_out) ;

    --------------------------- 7 segments control (input: pwm_width_bcd) ---------------------------
    simul_cond_s_true: if IS_SIMULATION generate
        seg_clk <= '1' ;
    end generate simul_cond_s_true;
    simul_cond_s_false: if (IS_SIMULATION = false) generate
        v_clk_s: Simple_Clk
            generic map (max_count => seg_maxcount)
            port map (clk_in => clk, reset => '0', clk_out => seg_clk) ;
    end generate simul_cond_s_false;

    hex_to_seven: Hex_to_7_Seg
        port map(hex => displayed_bcd, seven_seg => seg_out) ;
    
    enable_seg <= enable_seg_reg ;
    
    clk_seg: process(clk)
    begin
        if (rising_edge(clk)) then if (seg_clk = '1') then
            enable_seg_reg <= enable_seg_reg(num_digit-2 downto 0) & enable_seg_reg(num_digit-1) ;
        end if ; end if ;
    end process clk_seg ;
    
    seg_proc: process(enable_seg_reg, pwm_width_bcd, show_hex, pwm_width_rel)
    begin
        dot <= '1' ;
        displayed_bcd <= (others => '0') ;
    
        for m in 0 to num_digit-1 loop
            if (enable_seg_reg(m) = '0') then
                if (show_hex = '0') then
                    displayed_bcd <= std_logic_vector(pwm_width_bcd(num_digit*(m+1)-1 downto num_digit*m)) ;
                    
                    if (dot_pos = m) then
                        dot <= '0' ;
                    end if ;
                else
                    if (width_rel_size >= num_digit*(m+1)) then
                        displayed_bcd <= std_logic_vector(pwm_width_rel(num_digit*(m+1)-1 downto num_digit*m)) ;
                    elsif (width_rel_size > num_digit*m) then
                        displayed_bcd <= (others => '0') ;
                        displayed_bcd(width_rel_size-num_digit*m-1 downto 0) <= std_logic_vector(pwm_width_rel(width_rel_size-1 downto num_digit*m)) ;
                    end if ;
                end if ;
            end if ;
        end loop ;
    end process seg_proc ;

end Behavioral;
