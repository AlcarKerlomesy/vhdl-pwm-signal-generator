library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.ALL;
use IEEE.NUMERIC_STD.ALL;

package pwm_counter_proc is
    type t_PWM_COUNT is record
        pwm_maxcount, pwm_count_min, pwm_count_inc, pwm_rep_1, pwm_rep_2 : integer ;
    end record ;

    function constant_increment (
        clk_freq, pwm_freq, pwm_min_ratio, pwm_max_ratio : real ;
        pwm_resolution : integer) return t_PWM_COUNT ;
        
    function variable_increment (
        clk_freq, pwm_freq, pwm_min_ratio, pwm_max_ratio : real ;
        pwm_resolution : integer) return t_PWM_COUNT ;
end package pwm_counter_proc ;

package body pwm_counter_proc is
    function constant_increment(
        clk_freq, pwm_freq, pwm_min_ratio, pwm_max_ratio : real ;
        pwm_resolution : integer) return t_PWM_COUNT is
        
        constant pwm_maxcount_r : real := round(clk_freq / pwm_freq) ;
        constant pwm_count_min_r : real := ceil(pwm_maxcount_r*pwm_min_ratio) ;
        constant pwm_count_max_r : real := floor(pwm_maxcount_r*pwm_max_ratio) ;
        constant pwm_count_inc_r : real := floor((pwm_count_max_r - pwm_count_min_r) / real(pwm_resolution)) ;
        
        variable output : t_PWM_COUNT ;
    begin
        output.pwm_maxcount := integer(pwm_maxcount_r) ;
        output.pwm_count_inc := integer(pwm_count_inc_r) ;
        output.pwm_count_min := integer(round((pwm_count_min_r + pwm_count_max_r - pwm_count_inc_r*real(pwm_resolution))/2.0)) ;
        output.pwm_rep_1 := 1 ;
        output.pwm_rep_2 := 0 ;
        
        return output ;
    end function constant_increment ;
    
    function variable_increment(
        clk_freq, pwm_freq, pwm_min_ratio, pwm_max_ratio : real ;
        pwm_resolution : integer) return t_PWM_COUNT is
        constant pwm_maxcount_r : real := round(clk_freq / pwm_freq) ;
        constant pwm_count_min_r : integer := integer(ceil(pwm_maxcount_r*pwm_min_ratio)) ;
        constant pwm_count_range : integer := integer(floor(pwm_maxcount_r*pwm_max_ratio)) - pwm_count_min_r ;
        constant pwm_count_inc_r : integer := pwm_count_range / pwm_resolution ;
        constant count_rem : integer := pwm_count_range mod pwm_resolution ;
        
        variable pwm_rep_1_p1 : integer := 1 ;
        variable pwm_rep_2_p1 : integer ;
        variable pwm_rep_1_p2 : integer := 0 ;
        variable pwm_rep_2_p2 : integer := 1 ;
        
        variable output : t_PWM_COUNT ;
    begin
        if count_rem = 0 then
            output.pwm_rep_1 := 1 ;
            output.pwm_rep_2 := 0 ;
        else
            while true loop
                if (pwm_resolution - count_rem) mod pwm_rep_1_p1 = 0 then
                    if (count_rem*pwm_rep_1_p1) mod (pwm_resolution - count_rem) = 0 then
                        pwm_rep_2_p1 := (count_rem*pwm_rep_1_p1) / (pwm_resolution - count_rem) ;
                        exit ;
                    end if ;
                    if (count_rem*pwm_rep_1_p1) mod (pwm_resolution - count_rem - pwm_rep_1_p1) = 0 then
                        pwm_rep_2_p1 := count_rem*pwm_rep_1_p1 / (pwm_resolution - count_rem - pwm_rep_1_p1) ;
                        exit ;
                    end if ;
                else
                    if count_rem mod ((pwm_resolution - count_rem) / pwm_rep_1_p1) = 0 then
                        pwm_rep_2_p1 := count_rem / ((pwm_resolution - count_rem) / pwm_rep_1_p1) ;
                        exit ;
                    end if ;
                end if ;
                
                pwm_rep_1_p1 := pwm_rep_1_p1 + 1 ;
            end loop ;
            
            while pwm_rep_1_p2 + pwm_rep_2_p2 < pwm_rep_1_p1 + pwm_rep_2_p1 loop
                if count_rem mod pwm_rep_2_p2 = 0 then
                    if pwm_rep_2_p2 = pwm_rep_2_p1 then
                        pwm_rep_1_p2 := pwm_rep_1_p1 ;
                        exit ;
                    end if ;
                else
                    if (pwm_resolution - count_rem) mod (count_rem / pwm_rep_2_p2 + 1) = 0 then
                        pwm_rep_1_p2 := (pwm_resolution - count_rem) / (count_rem / pwm_rep_2_p2 + 1) ;
                        exit ;
                    end if ;
                end if ;
                
                pwm_rep_1_p2 := (pwm_resolution - count_rem) / (count_rem / pwm_rep_2_p2 + 1) + 1 ;
                pwm_rep_2_p2 := pwm_rep_2_p2 + 1 ;
            end loop ;
            
            if pwm_rep_1_p2 + pwm_rep_2_p2 < pwm_rep_1_p1 + pwm_rep_2_p1 then
                output.pwm_rep_1 := pwm_rep_1_p2 ;
                output.pwm_rep_2 := pwm_rep_2_p2 ;
            else
                output.pwm_rep_1 := pwm_rep_1_p1 ;
                output.pwm_rep_2 := pwm_rep_2_p1 ;
            end if ;
        end if ;
        
        output.pwm_maxcount := integer(pwm_maxcount_r) ;
        output.pwm_count_inc := integer(pwm_count_inc_r) ;
        output.pwm_count_min := integer(pwm_count_min_r) ;
        
        return output ;
    end function variable_increment ;
end package body pwm_counter_proc ;
