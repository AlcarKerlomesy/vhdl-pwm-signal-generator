library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Generate_PWM is
    Generic (
        max_count : integer := 100 ;     -- the clock frequency is 100 MHz / max_count
        min_count : integer := 5 ;
        inc_count : integer := 2 ;     -- The pulse width (in clock cycle) is min_count + falling_edge_count*inc_count
        rep_1_count : integer := 1 ;
        rep_2_count : integer := 0 ;
        bit_depth : integer := 16 );
    Port ( falling_edge_count : in unsigned (bit_depth-1 downto 0);
           clk : in STD_LOGIC ;
           reset : in STD_LOGIC ;
           PWM_out : out STD_LOGIC);
end Generate_PWM;

architecture Behavioral of Generate_PWM is
    constant bit_depth_T : integer := integer(ceil(log2(real(max_count+1)))) ;
    constant bit_depth_min : integer := integer(ceil(log2(real(min_count+1)))) ;
    constant bit_depth_inc : integer := integer(ceil(log2(real(inc_count+1)))) ;
    constant bit_depth_r : integer := integer(ceil(log2(realmax(real(rep_1_count+1), real(rep_2_count+1))))) ;
    
    signal counter_T : unsigned (bit_depth_T-1 downto 0) := (others => '0') ;
    signal counter_min : unsigned (bit_depth_min-1 downto 0) := (others => '0') ;
    signal counter_inc : unsigned (bit_depth_inc-1 downto 0) := (others => '0') ;
    signal counter_big : unsigned (bit_depth-1 downto 0) := (others => '0') ;
    signal counter_r : unsigned (bit_depth_r-1 downto 0) := (others => '0') ;
    signal switch_r : std_logic := '0' ;
    signal PWM_reg : std_logic := '0' ;
begin
    PWM_out <= PWM_reg ;

    counter_proc: process(clk)
    begin
        if (falling_edge(clk)) then
            counter_T <= counter_T + 1 ;
            counter_min <= counter_min ;
            counter_big <= counter_big ;
            counter_r <= counter_r ;
            switch_r <= switch_r ;
            PWM_reg <= PWM_reg ;
            
            if (counter_T = 0) then
                PWM_reg <= not reset ;
            end if ;
            
            if (min_count = 0) or (counter_min = min_count-1) then
                counter_inc <= counter_inc + 1 ;
                
                if (rep_1_count = 0 or rep_2_count = 0) then
                    if (counter_inc = inc_count-1) then
                        counter_big <= counter_big + 1 ;
                        counter_inc <= (others => '0') ;
                    end if ;
                else
                    if (switch_r = '0' and counter_inc = inc_count-1) or
                        (switch_r = '1' and counter_inc = inc_count) then
                        counter_big <= counter_big + 1 ;
                        counter_r <= counter_r + 1 ;
                        counter_inc <= (others => '0') ;
                        
                        if (switch_r = '0' and counter_r = rep_1_count-1) or
                            (switch_r = '1' and counter_r = rep_2_count-1) then
                            counter_r <= (others => '0') ;
                            switch_r <= not switch_r ;
                        end if ;
                    end if ;
                end if ;
                
                if (counter_big >= falling_edge_count) then
                    PWM_reg <= '0' ;
                end if ;
            else
                counter_min <= counter_min + 1 ;
            end if ;
            
            if (counter_T = max_count - 1) then
                counter_T <= (others => '0') ;
                counter_min <= (others => '0') ;
                counter_inc <= (others => '0') ;
                counter_big <= (others => '0') ;
                counter_r <= (others => '0') ;
                switch_r <= '0' ;
            end if ;
        end if ;
    end process counter_proc ; 
end Behavioral;
